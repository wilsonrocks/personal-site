let paths = [];

describe('checks for all pages', () => {
  before(() => {
    cy.request('/sitemap.xml').then(request => {
      const sitemap = request.body;
      const parser = new DOMParser();
      const xml = parser.parseFromString(sitemap, 'text/xml');
      const locNodes = xml.getElementsByTagName('loc');
      const urls = [...locNodes].map(node => node.textContent);
      paths = urls.map(url => new URL(url).pathname);
    });
  });

  it('check for dead links', () => {
    paths.forEach(path => {
      cy.log(`Let's check the links on page ${path}`);
      cy.visit(path);
      cy.get('a').each(link => {
        const path = link.attr('href');
        cy.request(path).then(response => {
          expect(response.isOkStatusCode).to.equal(true);
        });
      });
    });
  });

  it('passes accessibility', () => {
    paths.forEach(path => {
      cy.visit(path);
      cy.injectAxe();
      cy.checkA11y();
    });
  });
});
