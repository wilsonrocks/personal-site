const dateFormatter = new Intl.DateTimeFormat('en', { dateStyle: 'medium' });

syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight');

module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy('src/css');
  eleventyConfig.addPlugin(syntaxHighlight);

  eleventyConfig.addFilter('formatDate', dateFormatter.format);

  eleventyConfig.addFilter('yyyymmdd', function(date) {
    return date.toISOString().slice(0, 10);
  });

  eleventyConfig.setFrontMatterParsingOptions({ excerpt: true });
  eleventyConfig.setDataDeepMerge(true);

  const md = require('markdown-it')({
    html: false,
    breaks: true,
    linkify: true,
  });

  eleventyConfig.addNunjucksFilter('markdownify', markdownString => {
    if (typeof markdownString === 'string') return md.render(markdownString);
    return '';
  });

  eleventyConfig.addShortcode('license', () => {
    return `<div class="license">This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</div>`;
  });

  return {
    dir: {
      input: 'src',
    },
  };
};
