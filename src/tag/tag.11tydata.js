module.exports = {
  eleventyComputed: {
    description: data => `Posts by James Wilson about ${data.tag}`,
    title: data => `Posts about ${data.tag}`,
  },
};
