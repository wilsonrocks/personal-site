---
layout: page.hbs
title: My Projects
description: Various projects that I've worked on. They are all done in spare time, as my day job is almost all closed source
---

## Parent Quest

Console RPG themed gift for friends with a baby.

Vanilla JS, HTML, CSS with 11ty as a build tool.

This was fun to make! It was a gift for my friend [Zeph Auerbach](https://zephauerbach.com/) on the birth of his child. As they progress in levels, I send them small care packages.

I would say that if you want to make a vanilla JS app you need to be _very_ disciplined about your code. One of the benefits of frontend frameworks is forcing this discipline on you. I also really missed React's top down data flow - manually updating the DOM when values change is _boring_.

[Try it here](https://parent-quest.netlify.app/) or [View source](https://gitlab.com/wilsonrocks/parent-quest)

## Shelley Connect

Clone of the BBC quiz show [Only Connect](https://www.bbc.co.uk/programmes/b00lskhg) connecting wall round.

React, Framer Motion.

Built as part of a present for my wife's 40th birthday. Very pleased with the look and feel, the logic could be cleaner.

[Try it here](https://shelley-connect.netlify.app/familyQuiz) or [View source](https://gitlab.com/wilsonrocks/only-connect)

## QuizOrganised Mess

Site for me to use to run a weekly lockdown quiz for my family

Gatsby, React

Generates questions and answers from JSON files, supports video and audio questions. Doesn't look as slick as I'd like, but as the main user is me, sharing my screen, it doesn't matter. A good introduction to working with data and gatsby and graphQl. Not convinced my `gatsby-node.js` is something to be proud of, but once it was working, it wasn't worth going back to change it. My family really enjoyed it!

[Try it here](https://quizorganised-mess.netlify.app) or [View source](https://gitlab.com/wilsonrocks/family-quizzes)
