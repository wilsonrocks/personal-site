---
title: 'Blog posts delayed due to baby arrival'
tags:
  - personal
tldr: "Haven't posted for a while because we had a baby, but will be restarting soon."
description: 'I had planned to keep blogging uninterrupted by the birth of my third child, but it was not to be.'
date: 2020-10-06
---

Of course we knew the baby was coming.

And I thought that I would have a backlog of blog posts built up by then and just log in to gitlab on my phone and merge the branches so [netlify](https://netlify.app) could run [eleventy](https://11ty.dev) to build them.

But it was not to be.

---

I'm aware that having a blog with two previous posts of actual content, then a long gap, then a 'sorry for not posting more' post is not a _great_ look.

But it's happened. I've been lucky enough to have four weeks off (UK - two weeks paternity leave, two weeks shared parental leave) and I've loved it. I love coding, but a newborn baby is pretty special.

I've had an informal two-weekly posting schedule and kept to it so far. I'd planned to have a backlog, but preparing for the baby (we have two children already) while trying to get paid work into a position where I was comfortable leaving it was hard.

I have managed to do _one_ web project during paternity leave - a gift for a friend who has also had a baby. If they give me their permission, I'm going to write up what it was like making a vanilla JS game when you're _very_ used to using React and show people.

It's not an excuse - an excuse is not needed, babies are important!

More posts coming soon!

P.S. I'm under no illusions that this blog has any readership at all, let alone one craving my content while I've been off. I just wanted to post something.
