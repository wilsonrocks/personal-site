---
title: 'Throwing an error as a placeholder in typescript development'
tags:
  - typescript
type: top tip
tldr: "If a function throws an error, typescript won't complain about it not returning what it is meant to return"
description: 'Making one aspect of typescript development a bit nicer.'
date: 2020-08-10
---

Note - this post uses typescript 3.9.7 - error messages may vary in older versions.

While working on the same project as in my post about [`console.log`](/posts/level-up-your-console-log-game/), I came upon a small frustration.

```typescript
function noReturn(): number {}
```

rightly throws the error --- <samp>error TS2355: A function whose declared type is either 'void' nor 'any' must return a value.</samp>

Most of the time this is useful, but when you're starting a project, or in my case, porting a complicated inherited class, you might want to just give all the methods types _before_ implementing them.

It's also more useful in the case where it shows you when some particular logic path doesn't return the type it should, like this contrived example,

```typescript
function onePathReturns(willReturn: boolean): number {
  if (willReturn) return 5;
}
```

which throws <samp>error TS2366: Function lacks ending return statement and return type does not include 'undefined'.</samp>

These errors can be frustrating, though, because your code will not compile. And if your tests are also written in typescript (this project uses [ts-jest](https://kulshekhar.github.io/ts-jest/)), the tests won't even compile, meaning you can't see whether they are passing or not.

As this project had a lot of calculations, a lot of these methods would return numbers. So, I tried just returning a constant value. But weeks later, I found that a method I'd written was broken because another method I was calling was always returning 8, I found:

```typescript
method(argument: number): number {
  return 8; // TODO implement
}
```

which I found funny. After posting a screenshot to the team chat, I switched to returning `NaN`.

But the problem there, was that you _can_ get `NaN` through errors of your own. Also, this particular project returns `NaN` on purpose in some places. So you run the risk of the _always returns 8_ problem recurring.

Then I tried:

```typescript
method(argument: number): number {
  throw new Error('Not Yet Implemented!');
}
```

and... it compiled! Rather helpfully, the typescript compiler knows that it doesn't matter that you don't have a return statement, because it'll never be reached.

Not only that, but when you try to call it, in my case by running ts-jest, you get a nice test fail, telling you that it is failing because you haven't implemented it yet, _with a stack trace that tells you exactly where the problem is_.

You can also use this to close off a certain logic branch that isn't implemented yet, while you work on the other case:

```typescript
function oneBranchReturnsOneBranchThrows(willReturn: boolean): number {
  if (willReturn) return 5;
  throw new Error('not yet implemented');
}
// Compiles!
```

which was a great help in my project.

Even better, wherever else in your code you call `method`, the compiler will assign the output its correct type - so in this case, the return type of `oneBranchReturnsOneBranchThrows` will be treated as a number and all will be well. (I mean, we can't actually _run_ the code because it throws an error, of course, but it works as a placeholder).

_Side note: I haven't used them yet, but it looks like [@ts-expect-error comments](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-9.html#-ts-expect-error-comments) might cover some of the same ground. But they seem like they serve a slightly different purpose - more like something you leave permanently in your code, to allow runtime JS checks without the compiler complaining, rather than a placeholder to let you keep developing at a decent pace._
