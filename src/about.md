---
layout: page.hbs
title: About me
description: 'Former math teacher and union official turned web developer'
---

I am a web developer at [Gallagher (formerly AHC)](https://www.ajg.com/employeeexperience/insights/).

I used to be a math(s) teacher and a trade union official.

I write a _lot_ of javascript at work, and this blog is to share some of the things I've learned. I use React heavily at work, and for a change I wanted to write something with minimal javascript.

The easiest way to contact me is via email. I like hearing from people.

<address class ="myemail">jameswilson@hey.com</address>

I prefer using [gitlab](https://gitlab.com/wilsonrocks) to [github](https://github.com/wilsonrocks).

I have open sourced [this blog's code](https://gitlab.com/wilsonrocks/personal-site).
